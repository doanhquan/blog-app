<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // \App\User::truncate();
        \App\User::create([
            'name' => 'Account 1',
            'email' => 'admin@gmail.com',
            'password' => \Illuminate\Support\Facades\Hash::make(123456)
        ]);
        \App\User::create([
            'name' => 'Account 2',
            'email' => 'manager@gmail.com',
            'password' => \Illuminate\Support\Facades\Hash::make(123456)
        ]);   
    }
}
